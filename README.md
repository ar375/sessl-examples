# SESSL Examples

These are some usage examples of the experiment specification language [SESSL](https://git.informatik.uni-rostock.de/mosi/sessl). 
Each example experiment is located in a subfolder in this repository and can be executed with the corresponding scripts (`run.sh` on Unix and `run.bat` on Windows).

## Experiment Design

This example shows how model paramter combinations can be generated in various ways.
The code in this listing yields 900 parameter configurations by crossing 1 fixed value for a, 2 values for b, a range of 10 values (0.1, 0.2, ..., 1.0) for c, 5 points in a randomized Latin Hypercube design for d and e, and 9 points in a 2D central composite design for f and g.
    
    set("a" <~ 1.0)
    scan("b" <~ ("on", "off"))
    scan("c" <~ range(0.1, 0.1, 1.0))
    lhc(numPoints = 5)("d" <~ interval(0, 5), "e" <~ interval(0.0, 10.0))
    centralComposite("f" <~ interval(1, 2), "g" <~ interval(3, 4))

## Expression Observation

This example shows how observables can be aggregated via functions to new observables. 
The following lines specify that the number of sheep and wolf entities shall be observed as well as the sum of sheep and wolf. 
All three observations are recorded at each simulation time point 0, 1, 2, ..., 100.

    observe("s" ~ count("Sheep"))
    observe("w" ~ count("Wolf"))
    observe("s_and_w" ~ Expr(V => V("s") + V("w")))
    observeAt(range(0, 1, 100))
    
## Meta-modeling

This example shows an example for constructing a meta-model from simulation observations.
The model and the experiment are [taken](https://git.informatik.uni-rostock.de/mosi/jasss2018) from the following publication:

Reinhardt, O., Hilton, J. D., Warnke, T., Bijak, J., Uhrmacher, A. M. (2018):
[Streamlining simulation experiments with agent-based models in demography](http://jasss.soc.surrey.ac.uk/21/3/9.html).
 Journal of Artificial Societies and Social Simulation 21 (3) 9 
    
## Multi-Level Observation

This example showcases the syntax for observing specific entities in simulation runs of an ML-Rules model. 
The following lines specify the observation of different enitities by constraining their attributes or their position in the entity tree.

    observe("b4" ~ count("B(4.0)"))
    observe("b5" ~ count("B(5.0)"))
    observe("anyB" ~ count("*/B(_)"))
    observe("anyBInAnyA" ~ count("A(_)/B(_)"))
    observe("anyB2InAnyA" ~ count("A(_)/B(2.0)"))
    observe("anyB2InAnyAx" ~ count("A(x)/B(2.0)")) 

## Statistical Model-Checking

This example shows how Statistical Model-Checking can be performed with SESSL. 
For an SIR model defined in ML3, a number of simulation runs is executed automatically until the defined property can be proven correct or wrong (given some statistical error bounds). 
The following lines define a statistical test procedure as well as a property in temporal logic.

    test = SequentialProbabilityRatioTest(
        p = 0.8,
        alpha = 0.05,
        beta = 0.05,
        delta = 0.05)

    prop = MITL(
      // the number of infected people is above 400 at least once between times 20 and 30,
      // but reaches 0 between 120 and 180 time units after that
      F(20, 30)(
        (OutVar("inf") > Constant(400)).U(0, 0)(F(120, 180)(OutVar("inf") == Constant(0)))
      )
    )
    
## Replication Conditions

This example showcases complex replication conditions in SESSL. 
The following lines specify that runs shall be executed for each model configuration until the 99%-confidence interval of the value of the model output, observed at the last simulation time point, has a relative half-width of less than 1%.
Runs are executed in batches of 3.

    batchSize = 3

    replicationCondition = MeanConfidenceReached(
        confidence = 0.99,
        relativeHalfWidth = 0.01,
        varName = "a")
      
    observe("a" ~ count("A"))
    observeAt(stopTime)

## Result Processing

This example shows how the results of simulation runs can be processed directly in SESSL.
The following code snippet obtains and prints all time points with more observed wolfs than sheep.

    withRunResult { result =>
        val sheep = result.trajectory[Double]("s").toMap
        val wolfs = result.trajectory[Double]("w").toMap//
        
        // determine the time points with more wolfs than sheep
        val moreWolfs = sheep.keys.filter(t => sheep(t) < wolfs(t)).toSeq.sorted
        println(moreWolfs.mkString("\n"))
    }
    
## Sensitivity Analysis

This example shows how the sensitivity of a model's output against its inputs can be analyzed automatically.

## Stop Conditions

This example shows how a custom stop condition can be defined.
Using the following snippet, each simulation run will continue until all agents are dead or the simulation time exceeds 100. 

    stopCondition = Custom((state, time) => state.getAgentsAlive.isEmpty || time >= 100)
    
The same can be achieved with the following snippet:

    stopCondition = Custom((state, time) => state.getAgentsAlive.isEmpty) or AfterSimTime(100)
    
## Wnt optimization

This example shows how a complex simulation model can be tuned to reproduce a certain observation.
The paramaters of the model are systematically changed by an optimization algorithm that minimizes the distance between the model output and some reference values.
The model and the experiment are based on the following publication:

Haack F, Lemcke H, Ewald R, Rharass T, Uhrmacher AM (2015) [Spatio-temporal Model of Endogenous ROS and Raft-Dependent WNT/Beta-Catenin Signaling Driving Cell Fate Commitment in Human Neural Progenitor Cells](https://doi.org/10.1371/journal.pcbi.1004106). PLOS Computational Biology 11(3): e1004106.
