Migrant(
    skill: real,
    strategy: {"settler", "saver"},
    savingsTarget: real := 0,
    incomeTarget: real := 0,
    savings: real := 0,
    income: real := 0,
    employed: bool,
    ageOfUnemployment: real,
    ageOfIncomeChange: real,
    migrationAge: real
);

Universe();

// Parameters:
// m - number of initial ties
// z - period of waiting before unsuccessful migrants return
// theta - proportion of savers
// pi0 \
//       Job finding parameters
// pi1 / 
// alpha \
// beta	Income function parameters.
// gamma /
// tau - heterogeneity in skills
// mu - mean of savings target 
// sigma  - variance of migrant savings target
// C_h -  minimum desired earnings

network:Migrant[0-]<->[0-]Migrant:network;

Migrant.return() -> 
    for each ?neighbor in ego.network
      ?neighbor.network -= ego,
      ?neighbor.setIncome()
    end ,
    ego.die();
    
Migrant.secondNeighbours() := (ego.network.collect(alter.network) - ego.network);

Migrant.jobFindingRate() := 
	(pi0 - pi1 * Migrant.all.size()*  Migrant.all.size()) * (1 + ego.secondNeighbours().filter(alter.employed).size() / 
		 (1 + ego.secondNeighbours().filter(!alter.employed).size()));
	

 
Migrant.setIncome() ->
    if ego.employed then
      if ego.income >= ego.incomeTarget && ?newIncome < ego.incomeTarget then
        ego.ageOfIncomeChange := ego.age
      end,
      ego.income := ?newIncome
    end
  where ?newIncome := max(0.01, 
                        ((alpha - beta * Migrant.all.size() * Migrant.all.size()) * ego.skill) // earnings part
                          +  gamma * log(1 + ego.network.size())); // network part


Universe
//    @ constantInflowRate // start with constant rate, later maybe dependent on overall size of the population
    @ ego.calculateInflowRate()
    -> ?migrant := new Migrant(
         skill := if tau > 0 then max(0.001, normal(1,tau)) else 1,
         strategy := if random() < theta then "saver" else "settler",
         age := ?ageAtMigration,
         ageOfUnemployment := ?ageAtMigration,
         ageOfIncomeChange := ?ageAtMigration,
         migrationAge := ?ageAtMigration
       ),
       if (Migrant.all.size() > 1) then
         ?migrant.network += (Migrant.all - ?migrant).random(),
         ?migrant.network += (?migrant.network.only().network - ?migrant).random(poisson(m-1))
       end ,
       for each ?neighbor in ?migrant.network
         ?neighbor.setIncome()
       end ,
       if ?migrant.strategy = "settler" then
         ?migrant.incomeTarget := c_h,
         ?migrant.savingsTarget := 9999
       else
         ?migrant.incomeTarget := c_h * 0.5,
         ?migrant.savingsTarget := normal(mu, sigma)
       end 
    where ?ageAtMigration := ageAtMigration[random()]+random(); // load from file

Universe.calculateInflowRate() := (constantInflowRate *
							  exp((alpha - beta * Migrant.all.size() * Migrant.all.size()) - c_h));

Migrant
// Migrant returns.
    | ego.income < ego.incomeTarget
    @ age ego.ageOfIncomeChange + z
    -> ego.return();
    
    | !ego.employed
    @ age ego.ageOfUnemployment + z
    -> ego.return();
    
    | ego.strategy = "saver", ego.savings > ego.savingsTarget
    @ instantly
    -> ego.return();
    
// Find Job.
    | !ego.employed
    @ ego.jobFindingRate()
    -> ego.employed := true,
       ego.setIncome();

// exclude retired agents
    | 
    @ age 60
    -> ego.return();
       
// Payout day.
    @ every 1/12 synchronized 
    -> ego.savings += ego.income;