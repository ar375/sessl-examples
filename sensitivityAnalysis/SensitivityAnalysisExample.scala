package org.sessl

object SensitivityAnalysisExampleExperiment extends App {

  import sessl._
  import sessl.mlrules._
  import sessl.analysis.sensitivity._

  analyze((params, objective) =>
    execute {
      new Experiment with Observation {

        model = "configureInitialStateModel.mlrj"

        stopTime = 0

        val numA = observe(count("A"))
        observeAt(0)

        for ((name, value) <- params)
          set(name <~ value)

        withReplicationsResult {
          result => objective <~ result.mean(numA)
        }
      }
  }) using new OneAtATimeSetup {
    baseCase(
      "x" <~ 1,
      "y" <~ 1
    )
    sensitivityCase(
      "x" <~ 3,
      "y" <~ 4
    )
    withAnalysisResult(result => {
      val indX = result.individualEffect("x")
      val indY = result.individualEffect("y")
      val intXY = result.interactionEffect("x", "y")

      println(s"Individual(x) = $indX")
      println(s"Individual(y) = $indY")
      println(s"Interaction(x, y) = $intXY")
    })
  }
}
