package org.sessl

object ResultProcessingExampleExperiment extends App {

  import sessl._
  import sessl.mlrules._

  execute {
    new Experiment with Observation {
      model = "./prey-predator.mlrj"
      simulator = SimpleSimulator()

      stopTime = 100

      observe("s" ~ count("Sheep"))
      observe("w" ~ count("Wolf"))
      observeAt(range(0, 1, 100))

      withRunResult { result =>
        val sheep = result.trajectory[Double]("s").toMap
        val wolfs = result.trajectory[Double]("w").toMap//

        // determine the time points with more wolfs than sheep
        val moreWolfs = sheep.keys.filter(t => sheep(t) < wolfs(t)).toSeq.sorted
        println(moreWolfs.mkString("\n"))
      }
    }
  }
} 
