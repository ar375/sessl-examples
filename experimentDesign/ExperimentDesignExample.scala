package org.sessl

object ExperimentDesignExampleExperiment extends App {

  import sessl._
  import sessl.mlrules._
  import sessl.ssj.LHCSampling

  execute {
    new Experiment with CentralCompositeDesign with LHCSampling {
      model = "./prey-predator.mlrj"
      simulator = SimpleSimulator()

      stopTime = 0

      set("a" <~ 1.0)
      scan("b" <~ ("on", "off"))
      scan("c" <~ range(0.1, 0.1, 1.0))
      lhc(numPoints = 5)("d" <~ interval(0, 5), "e" <~ interval(0.0, 10.0))
      centralComposite("f" <~ interval(1, 2), "g" <~ interval(3, 4))

    }
  }
} 
