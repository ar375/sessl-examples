package org.sessl

object WntOptimizationExampleExperiment extends App {

  import java.lang.{System => JavaSystem}

  import sessl._
  import sessl.mlrules._
  import sessl.opt4j._
  import sessl.optimization._

  object WntProperties {
    val ref: Trajectory[Double] = List(
      (  0, 5282),
      (120, 7561),
      (240, 8247),
      (360, 7772),
      (480, 7918),
      (600, 7814),
      (720, 7702))
  }

  val startTime = JavaSystem.currentTimeMillis

  minimize { (params, objective) => // Minimize the following function
    execute {
      new Experiment with Observation with ParallelExecution {

        model = "wnt.mlrj"
        simulator = SimpleSimulator()
        parallelThreads = -1
        stopTime = 720
        replications = 7

        for ((input, value) <- params.values)
          set(input <~ value)

        set("kLA_diss" <~ params("diss")) //Set model parameters as defined by optimizer
        set("kLdephos" <~ params("dephos"))
        set("kLphos" <~ params("phos"))
        set("kWsyn" <~ params("syn"))

        val bInNuc = observe(count("Cell/Nuc/Bcat"))
        observeAt(range(0, 120, 720))

        withExperimentResult { results =>
          val distances = for (run <- results.runs) yield {
            val trajectory = run.trajectory(bInNuc)
            Misc.rmse(trajectory, WntProperties.ref)
          }
          objective <~ distances.sum / distances.size
        }
      }
    }
  } using new Opt4JSetup {
    param("diss", 5E-5, 5E-3) // Optimization parameter bounds
    param("dephos", 0.01, 0.1)
    param("phos", 0.1, 10)
    param("syn", 0.1, 5)
    optimizer = ParticleSwarmOptimization(iterations = 30, particles = 20)
    // showViewer = true //Switches on Opt4J GUI
    withOptimizationResults { results =>
      println("Overall results: " + results.head) //print results
    }
  }
} 
